-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 25, 2019 at 01:52 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sharina`
--

-- --------------------------------------------------------

--
-- Table structure for table `contributions`
--

CREATE TABLE `contributions` (
  `user_ID` int(10) NOT NULL,
  `amount` decimal(40,2) NOT NULL,
  `date` text NOT NULL,
  `event` varchar(200) NOT NULL,
  `transactioncode` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contributions`
--

INSERT INTO `contributions` (`user_ID`, `amount`, `date`, `event`, `transactioncode`) VALUES
(2, '100.00', 'Sun,Feb-10-2019  03:11:47', 'fees', 0x54525543354538444343354338373435303739304642),
(2, '5000.00', 'Sun,Feb-10-2019  03:53:56', 'Monthly', 0x54525546353737324133363038313131373036383734),
(3, '1500.00', 'Sun,Feb-10-2019  16:38:37', 'Monthly', 0x54525541434133363133374445413738423433303146),
(3, '500.00', 'Sun,Feb-10-2019  17:31:23', 'Moyo safi', 0x54525530353831384636443838303931394545463042),
(4, '5000.00', 'Fri,Feb-22-2019  17:17:18', 'School Fees Harambee', 0x54525546343035383342363238463243463435443838);

-- --------------------------------------------------------

--
-- Table structure for table `lending`
--

CREATE TABLE `lending` (
  `ID` int(10) NOT NULL,
  `user_ID` int(10) NOT NULL,
  `amount_requested` decimal(10,2) NOT NULL,
  `duration` int(5) NOT NULL,
  `payable` decimal(10,2) NOT NULL,
  `installment` decimal(10,2) NOT NULL,
  `date` text NOT NULL,
  `reason` text NOT NULL,
  `paid` int(1) NOT NULL DEFAULT '0',
  `remaining` decimal(10,2) NOT NULL DEFAULT '0.00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `lending`
--

INSERT INTO `lending` (`ID`, `user_ID`, `amount_requested`, `duration`, `payable`, `installment`, `date`, `reason`, `paid`, `remaining`) VALUES
(1, 2, '10000.00', 17, '16200.00', '1000.00', 'Sun,Feb-10-2019  03:12:48', 'School fees', 1, '0.00'),
(2, 3, '5000.00', 7, '6550.00', '1000.00', 'Sun,Feb-10-2019  16:43:40', 'School fees', 0, '5550.00'),
(3, 4, '50000.00', 7, '65500.00', '10000.00', 'Fri,Feb-22-2019  17:24:25', 'Some shit!', 0, '65500.00'),
(4, 2, '65000.00', 19, '108659.00', '6000.00', 'Fri,Feb-22-2019  17:40:31', 'Yes', 0, '100659.00');

-- --------------------------------------------------------

--
-- Table structure for table `moneypool`
--

CREATE TABLE `moneypool` (
  `ID` int(10) NOT NULL,
  `user_ID` int(10) DEFAULT NULL,
  `totalamount` decimal(15,2) NOT NULL DEFAULT '0.00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `moneypool`
--

INSERT INTO `moneypool` (`ID`, `user_ID`, `totalamount`) VALUES
(1, 2, '4400.00'),
(2, 3, '2000.00'),
(3, 4, '5000.00');

-- --------------------------------------------------------

--
-- Table structure for table `repays`
--

CREATE TABLE `repays` (
  `user_ID` int(10) NOT NULL,
  `loanID` int(10) NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `date` text NOT NULL,
  `remaining` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `repays`
--

INSERT INTO `repays` (`user_ID`, `loanID`, `amount`, `date`, `remaining`) VALUES
(2, 1, '16200.00', 'Sun,Feb-10-2019  03:33:51', '0.00'),
(3, 2, '1000.00', 'Sun,Feb-10-2019  17:33:22', '5550.00'),
(2, 4, '8000.00', 'Fri,Feb-22-2019  17:03:34', '100659.00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `ID` int(10) NOT NULL,
  `firstname` varchar(20) NOT NULL,
  `lastname` varchar(20) NOT NULL,
  `username` varchar(20) NOT NULL,
  `userLevel` varchar(10) NOT NULL,
  `email` varchar(100) NOT NULL,
  `nationalID` int(8) NOT NULL,
  `password` varchar(355) NOT NULL,
  `phone` int(11) NOT NULL,
  `reg_date` text NOT NULL,
  `reg_fee` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`ID`, `firstname`, `lastname`, `username`, `userLevel`, `email`, `nationalID`, `password`, `phone`, `reg_date`, `reg_fee`) VALUES
(1, 'Admin', 'Admin', 'Admin', 'admin', 'admin@sharina.local', 0, '$2y$11$T/SYagL6XWakdcx1AuIJXeTX..WYYsvCjaYXf0BN3FgJULd1VAcTe', 700022757, 'Wed, May 05 2017 @ 09:35:45', 200),
(3, 'Kevin', 'Machimbo', 'kmachy', 'user', 'kmachy94@gmail.com', 25279790, '$2y$11$EpvHcGpubqnHjASGoe/JnOky2TV8OioYd4qMNceboDbPnXZfY1hs2', 716852018, 'Sun, Feb 02 2019 @ 16:35:37', 200),
(4, 'Amri', 'Ojuka', 'laktar', 'user', 'aaojuka@gmail.com', 25826116, '$2y$11$creulXp4yDsC.oWMUyoIrOA2I.YRFjQnWbPOeockxX6PxzfNCjXAC', 748302641, 'Fri, Feb 02 2019 @ 17:16:50', 200),
(2, 'Charles', 'Otieno', 'trulance', 'user', 'ogegoe@gmail.com', 27259790, '$2y$11$zB9eXZJf9vl.YScr0J.IDe5T6Ofnkv1Or6m1.i8vaX4Xoao3BpLJS', 791036665, 'Sun, Feb 02 2019 @ 03:46:29', 200);

-- --------------------------------------------------------

--
-- Table structure for table `withdrawals`
--

CREATE TABLE `withdrawals` (
  `user_ID` int(10) NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `date` text NOT NULL,
  `reason` varchar(200) NOT NULL,
  `transactioncode` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `withdrawals`
--

INSERT INTO `withdrawals` (`user_ID`, `amount`, `date`, `reason`, `transactioncode`) VALUES
(2, '100.00', 'Sun,Feb-10-2019  03:44:48', 'pocket', 0x54525538303835303544334535354437333434364637),
(2, '600.00', 'Sun,Feb-10-2019  03:25:57', 'urgent', 0x54525546433632353043313530453631383936353644);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `contributions`
--
ALTER TABLE `contributions`
  ADD KEY `contributions_ibfk_1` (`user_ID`);

--
-- Indexes for table `lending`
--
ALTER TABLE `lending`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FK_user_ID` (`user_ID`);

--
-- Indexes for table `moneypool`
--
ALTER TABLE `moneypool`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `user_ID` (`user_ID`);

--
-- Indexes for table `repays`
--
ALTER TABLE `repays`
  ADD KEY `repays_USER_ID` (`user_ID`),
  ADD KEY `FK_loam_ID` (`loanID`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD UNIQUE KEY `nationalID` (`nationalID`),
  ADD KEY `ID` (`ID`),
  ADD KEY `userLever` (`userLevel`);

--
-- Indexes for table `withdrawals`
--
ALTER TABLE `withdrawals`
  ADD KEY `user_ID` (`user_ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `lending`
--
ALTER TABLE `lending`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `moneypool`
--
ALTER TABLE `moneypool`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `ID` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `contributions`
--
ALTER TABLE `contributions`
  ADD CONSTRAINT `contributions_ibfk_1` FOREIGN KEY (`user_ID`) REFERENCES `users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `lending`
--
ALTER TABLE `lending`
  ADD CONSTRAINT `lending_ibfk_1` FOREIGN KEY (`user_ID`) REFERENCES `users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `moneypool`
--
ALTER TABLE `moneypool`
  ADD CONSTRAINT `moneypool_ibfk_1` FOREIGN KEY (`user_ID`) REFERENCES `users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `repays`
--
ALTER TABLE `repays`
  ADD CONSTRAINT `repays_ibfk_1` FOREIGN KEY (`user_ID`) REFERENCES `users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `repays_ibfk_2` FOREIGN KEY (`loanID`) REFERENCES `lending` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `withdrawals`
--
ALTER TABLE `withdrawals`
  ADD CONSTRAINT `withdrawals_ibfk_1` FOREIGN KEY (`user_ID`) REFERENCES `users` (`ID`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
