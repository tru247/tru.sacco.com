<?php
$page_title  = "Contact Us";
include_once "includes/header.php";
?>


<div class="body">
    <div class="row">
        <div class="col-sm-4">
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h3 class="panel-title">TruSacco</h3>
                </div>
                <div class="panel-body">
                    <div class="col-sm-5">
                        Phone Number:<br/>
                        Address:<br/>
                        Email:<br/>
                        Facebook:&nbsp;<i class="glyphicon icon-facebook"></i><br/>
                    </div>
                    <div class="col-sm-7">
                        +254 791 036 665<br/>
                        36-00000 - Nyilima<br/>
                        <a href="mailto:trulance247@gmail.com">trulance247@gmail.com</a><br/>
                        <a href="https://fb.com/">www.facebook.com/trulanc247</a>
                    </div>
                </div>
            </div>
        </div><!-- /.col-sm-4 -->
        <div class="col-sm-4">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Charles E.O. Otieno</h3>
                </div>
                <div class="panel-body">
                    <div class="col-sm-6">
                        Phone Number:<br/>
                        Address:<br/>
                        Email Address:<br/>

                    </div>
                    <div class="col-sm-6">
                        +254 791 036 665<br/>
                        36-00000 - Nyilima<br/>
                        <a href="mailto:ogegoe@gmail.com">ogegoe@gmail.com</a>
                    </div>
                </div>
            </div>
        </div><!-- /.col-sm-4 -->
        <div class="col-sm-4">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Evelyne Auma</h3>
                </div>
                <div class="panel-body">
                    <div class="col-sm-6">
                        Phone Number:<br/>
                        Address:<br/>
                        Email Address:<br/>

                    </div>
                    <div class="col-sm-6">
                        +254 700 918 181<br/>
                        60401 - Chogoria<br/>
                        <a href="mailto:evah@gmail.com">evah@gmail.com</a>
                    </div>
                </div>
            </div>
        </div><!-- /.col-sm-4 -->
    </div>
</div>


<!-- footer -->
<?php
include_once "includes/footer.php";
?>
